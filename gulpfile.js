// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var stylus = require('gulp-stylus'),
	pug = require('gulp-pug'),
	iconfont = require('gulp-iconfont'),
	del = require('del'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	consolidate = require("gulp-consolidate"),
	livereload = require('gulp-livereload'),

	runTimestamp = Math.round(Date.now()/1000);


// Clean build folder
gulp.task('clean', function() {
	return del('build/**/*');
});


// Compile Pug
gulp.task('templates', function() {
	return gulp.src(['src/templates/**/*.pug', '!src/templates/**/_*.pug'])
		.pipe(pug({
			pretty: '\t'
		}))
		.pipe(gulp.dest('build'))
		.pipe(livereload({
			quiet: true
		}));
});


// Compile Stylus
gulp.task('stylus', function() {
	return gulp.src(['src/styles/*.styl', '!src/styles/_*.styl'])
		.pipe(stylus({
			compress: true
		}))
		.pipe(gulp.dest('build/css'))
		.pipe(livereload({
			quiet: true
		}));
});


// Concatenate & Minify JS
gulp.task('scripts', function() {
	return gulp.src(['src/scripts/*.js'])
		.pipe(concat('all.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/js'))
		.pipe(livereload({
			quiet: true
		}));
});


// Copy images
gulp.task('images', function() {
	return gulp.src(['src/images/*'])
		.pipe(gulp.dest('build/img'))
		.pipe(livereload({
			quiet: true
		}));
});


// Generate icon font
gulp.task('iconfont', function() {
	gulp.src(['src/fonts/icons/*.svg'])
		.pipe(iconfont({
			fontName: 'icons',
			prependUnicode: true,
			formats: ['ttf', 'eot', 'woff'],
			timestamp: runTimestamp,
			centerHorizontally: true,
			fontHeight: 1024,
			descent: 128,
			ascent: 1024 - 128,
		}))
		.on('glyphs', function(glyphs, options) {
			gulp.src('src/fonts/_icons.styl')
				.pipe(consolidate('lodash', {
					glyphs: glyphs,
					fontName: 'icons',
					fontPath: '../fonts/',
					className: 'icon'
				}))
			.pipe(gulp.dest('src/styles'));

			gulp.src('src/fonts/_preview.html')
				.pipe(consolidate('lodash', {
					glyphs: glyphs,
					fontName: 'icons',
					fontPath: '../fonts/',
					className: 'icon'
				}))
			.pipe(rename('icons-preview.html'))
			.pipe(gulp.dest('build/fonts'));
		})
		.pipe(gulp.dest('build/fonts'))
		.pipe(livereload({
			quiet: true
		}));
});



// Watch Files For Changes
gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('src/fonts/icons/*.svg', ['iconfont']);
	gulp.watch('src/scripts/*.js', ['scripts']);
	gulp.watch('src/templates/**/*.pug', ['templates']);
	gulp.watch('src/styles/*.styl', ['stylus']);
	gulp.watch('src/images/*', ['images']);
});


// Default Task
gulp.task('default', ['templates', 'iconfont', 'stylus', 'scripts', 'images', 'watch']);

