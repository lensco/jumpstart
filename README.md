# Lensco Jumpstart
A starting point for my new projects


## Requirements
- NPM
- Gulp

## First run
    $ npm install

## Build
    $ gulp
    
To clean up the `build` directory, run

    $ gulp clean

## Run from simple php server
    cd build
    php -S localhost:8000
   